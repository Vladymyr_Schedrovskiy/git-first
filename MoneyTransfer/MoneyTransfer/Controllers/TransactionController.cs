﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Localization;
using MoneyTransfer.Models;
using MoneyTransfer.Services;
using MoneyTransfer.ViewModels;

namespace MoneyTransfer.Controllers
{
    public class TransactionController : Controller
    {
        private readonly ITransactionService transactionService;
        private readonly UserManager<User> userManager;
        private readonly ApplicationContext context;
        private readonly IStringLocalizer<TransactionController> localizer;


        public TransactionController(ITransactionService transactionService, UserManager<User> userManager, ApplicationContext context, IStringLocalizer<TransactionController> localizer)
        {
            this.transactionService = transactionService;
            this.userManager = userManager;
            this.context = context;
            this.localizer = localizer;
        }


        [HttpGet]
        public ActionResult MoneyTransaction()
        {

            return View();
        }

        [HttpPost]
        public async Task<IActionResult> MoneyTransaction(MoneyTransactionViewModel model)
        {
            if (ModelState.IsValid)
            {
                if (await transactionService.IsAccountExist(model.RecipientAccount))
                {
                    User sender = await userManager.FindByNameAsync(HttpContext.User.Identity.Name);
                    if (await transactionService.IsEnoughMoney(model.Amount, sender.UserName))
                    {
                        User recipient = await userManager.FindByNameAsync(model.RecipientAccount);
                        Transaction transaction = new Transaction
                        {
                            SenderId = sender.Id,
                            RecepientId = recipient.Id,
                            Amount = model.Amount,
                            TransactionDate = DateTime.Now
                        };
                        sender.Balance -= model.Amount;
                        recipient.Balance += model.Amount;
                        await context.Transactions.AddAsync(transaction);
                        await userManager.UpdateAsync(sender);
                        await userManager.UpdateAsync(recipient);
                        await context.SaveChangesAsync();
                        return RedirectToAction("GetTransactions", "Transaction");
                    }
                    else
                    {
                        return Content(localizer["NotEnoughContent"]);
                    }
                }
                else
                {
                    return Content(localizer["NotExistContent"]);
                }
            }
            return View(model);
        }

        public async Task<IActionResult> GetTransactions()
        {
            User user = await userManager.FindByNameAsync(HttpContext.User.Identity.Name);
            List<Transaction> transactions = await transactionService.GetTransactions(user);
            return View(transactions);
        }





    }
}