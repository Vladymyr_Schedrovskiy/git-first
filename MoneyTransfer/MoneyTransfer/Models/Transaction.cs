﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;


namespace MoneyTransfer.Models
{
    public class Transaction
    {
        [Key]
        public int Id { get; set; }

        public string SenderId { get; set; }
        public User Sender { get; set; }

        public string RecepientId { get; set; }
        public User Recepient { get; set; }

        public int Amount { get; set; }

        public DateTime TransactionDate{ get; set; }


    }
}
