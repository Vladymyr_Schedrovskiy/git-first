﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Identity;
using Microsoft.CodeAnalysis.CSharp;
using MoneyTransfer.Models;

namespace MoneyTransfer.Services
{
    public interface IAccountGeneratorService
    {
        string GenerateUniqueAccount();
    }

    public class AccountGeneratorService : IAccountGeneratorService
    {
        private readonly UserManager<User> userManager;

        public AccountGeneratorService(UserManager<User> userManager)
        {
            this.userManager = userManager;
        }



        public int GenerateNumber()
        {
            Random rnd = new Random();
            int number = rnd.Next(100000, 1000000);
            return number;
        }

        private bool IsUnique(int account)
        {
            List<string> usersAccounts = userManager.Users.Select(u => u.UserName).ToList();
            bool isUnique = true;
            foreach (var uA in usersAccounts)
            {
                if (uA == account.ToString())
                {
                    isUnique = false;
                }
            }
            return isUnique;
        }

        public string GenerateUniqueAccount()
        {
            int number = 0;
            bool isUnique = false;
            while (!isUnique)
            {
                number = GenerateNumber();
                isUnique = IsUnique(number);
            }
            return number.ToString();
        }
        

    }
}