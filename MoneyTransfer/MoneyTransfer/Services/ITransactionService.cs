﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using MoneyTransfer.Models;

namespace MoneyTransfer.Services
{
    public interface ITransactionService
    {
      
        Task<bool> IsAccountExist(string recepientAccount);
        Task<bool> IsEnoughMoney(int amount, string senderAccount);
        
        Task<List<Transaction>> GetTransactions(User user);
    }

    public class TransactionService : ITransactionService
    {
        private readonly UserManager<User> userManager;
        private readonly ApplicationContext context;

        public TransactionService(UserManager<User> userManager, ApplicationContext context)
        {
            this.userManager = userManager;
            this.context = context;
        }

        public async Task<bool> IsAccountExist(string recepientAccount)
        {
            User user = await userManager.FindByNameAsync(recepientAccount);
            return user != null;
        }

        public async Task<bool> IsEnoughMoney(int amount, string senderAccount)
        {
            User user = await userManager.FindByNameAsync(senderAccount);
            return user.Balance > amount;
        }

       

        public async Task<List<Transaction>> GetTransactions(User user)
        {
            List<Transaction> transactions = 
                await context.Transactions.Where(t => t.RecepientId == user.Id || t.SenderId == user.Id).Include(t=>t.Sender).Include(t=>t.Recepient).ToListAsync();
            return transactions;
        }

       

    }
}
