﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace MoneyTransfer.ViewModels
{
    public class LoginViewModel
    {
        [Required]
        [Display(Name = "AccountDisplay")]
        public string Account { get; set; }

        [Required]
        [DataType(DataType.Password)]
        [Display(Name = "PasswordDisplay")]
        public string Password { get; set; }

        [Display(Name = "RememberMeDisplay")]
        public bool RememberMe { get; set; }

        public string ReturnUrl { get; set; }
    }
}
