﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace MoneyTransfer.ViewModels
{
    public class RegisterViewModel
    {
        [Required]
        [Display(Name = "EmailDisplay")]
        public string Email { get; set; }

        [Required]
        [DataType(DataType.Password)]
        [Display(Name = "PasswordDisplay")]
        public string Password { get; set; }

        [Required]
        [Compare("Password", ErrorMessage = "ConfirmErrorMessage")]
        [DataType(DataType.Password)]
        [Display(Name = "PassConfirmDisplay")]
        public string PasswordConfirm { get; set; }
    }
}
