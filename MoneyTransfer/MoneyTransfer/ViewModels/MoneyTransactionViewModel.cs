﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace MoneyTransfer.ViewModels
{
    public class MoneyTransactionViewModel
    {
        

        [Required]
        [Display(Name = "RecipientDisplay")]
        public string RecipientAccount { get; set; }

        [Required]
        [Display(Name = "AmountDisplay")]
        public int Amount { get; set; }
    }
}
