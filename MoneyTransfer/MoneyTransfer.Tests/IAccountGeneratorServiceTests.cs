﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.AspNetCore.Identity;
using MoneyTransfer.Models;
using MoneyTransfer.Services;
using Moq;
using Xunit;

namespace MoneyTransfer.Tests
{
    public class IAccountGeneratorServiceTests
    {

        [Fact]
        public void GenerateRandNumber()
        {
            var mock = new Mock<UserManager<User>>();
            mock.Setup(repo => repo.Users.Select(u => u.UserName).ToList()).Returns(GetAccounts());
            AccountGeneratorService generator = new AccountGeneratorService(mock.Object);
            string randNumber = generator.GenerateUniqueAccount();
            Assert.InRange(int.Parse(randNumber), 99999, 1000000);
            
        }

        private List<string> GetAccounts()
        {
            Random rnd = new Random();
            string[] accounts = new string[10000];
            for (int i = 0; i < 10000; i++)
            {

                accounts[i] = rnd.Next(100000, 1000000).ToString();
            }
            return accounts.ToList();
        }
    }
}
